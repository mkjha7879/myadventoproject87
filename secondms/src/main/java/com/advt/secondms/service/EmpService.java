package com.advt.secondms.service;

import java.util.List;

import com.advt.secondms.modal.Emp;

public interface EmpService {
	public Emp createEmp(Emp emp);
	public List<Emp> getAllEmp();
	public Emp getEmp(int id);
	public Emp updateEmp(int id,Emp emp);
	public void deleteEmp(int id);

}
