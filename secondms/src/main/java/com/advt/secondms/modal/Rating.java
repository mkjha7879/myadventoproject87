package com.advt.secondms.modal;

public class Rating {
	private String ratingId;
	private int empId;
	private String hotelId;
	private int rating;
	private String remarks;

	public Rating() {
		super();
	}

	public Rating(String ratingId, int empId, String hotelId, int rating, String remarks) {
		super();
		this.ratingId = ratingId;
		this.empId = empId;
		this.hotelId = hotelId;
		this.rating = rating;
		this.remarks = remarks;
	}

	public String getRatingId() {
		return ratingId;
	}

	public void setRatingId(String ratingId) {
		this.ratingId = ratingId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
