package com.advt.secondms.service_impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.advt.secondms.Repository.EmpRepository;
import com.advt.secondms.exception.ResourceNotFoundExcption;
import com.advt.secondms.modal.Emp;
import com.advt.secondms.service.EmpService;
@Service(value="EmpImpl1")
public class EmpServiceImpl implements EmpService {
	@Autowired
	private EmpRepository empRepository;

	@Override
	public List<Emp> getAllEmp() {
		
		List<Emp> findAll = empRepository.findAll();
		return findAll;
	}

	@Override
	public Emp createEmp(Emp emp) {
		Emp save = empRepository.save(emp);
		return save;
	}
	@Override
	public Emp getEmp(int id) {
		Emp emp = empRepository.findById(id).orElseThrow(()-> new ResourceNotFoundExcption("the id="+id+" is not present please go with another id"));
		return emp;
		
	}

	@Override
	public Emp updateEmp(int id,Emp emp) {
		Emp emp1 = empRepository.findById(id).orElseThrow(()-> new ResourceNotFoundExcption("the id="+id+" is not present please go with another id to update the value"));
		emp1.setId(id);
		emp1.setAge(emp.getAge());
		emp1.setJobLocation(emp.getJobLocation());
		emp1.setName(emp.getName());
		emp1.setSalary(emp.getSalary());
		Emp save = empRepository.save(emp1);
		return save;
	}

	@Override
	public void deleteEmp(int id) {
		Emp emp = empRepository.findById(id).orElseThrow(()-> new ResourceNotFoundExcption("the id="+id+" is not present please go with another id to delete the value"));
		empRepository.delete(emp);
		
		
	}
	
	

}
