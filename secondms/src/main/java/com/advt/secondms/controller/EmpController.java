package com.advt.secondms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advt.secondms.modal.Emp;
import com.advt.secondms.service.EmpService;

@RestController
@RequestMapping("/Emp")
public class EmpController {
	@Autowired
	@Qualifier(value = "EmpImpl1")
	private EmpService empService;

	@PostMapping("/add")
	public ResponseEntity<Emp> create(@RequestBody Emp emp) {
		Emp createEmp = empService.createEmp(emp);
		return new ResponseEntity<Emp>(createEmp, HttpStatus.CREATED);
	}

	@GetMapping("/getAll")
	public ResponseEntity<List<Emp>> getAll() {
		List<Emp> allEmp = empService.getAllEmp();
		return new ResponseEntity<List<Emp>>(allEmp, HttpStatus.OK);
	}
	@GetMapping("/get/{empId}")
	public ResponseEntity<Emp> getEmp(@PathVariable int empId) {
		Emp emp = empService.getEmp(empId);
		return new ResponseEntity<Emp>(emp,HttpStatus.OK);
	}
	@PutMapping("/update/{empId}")
	public ResponseEntity<Emp> updateEmp(@PathVariable int empId,@RequestBody Emp emp) {
		Emp emp2 = empService.updateEmp(empId, emp);
		return new ResponseEntity<Emp>(emp2,HttpStatus.ACCEPTED);
		
	} 
	@DeleteMapping("/delete/{empId}")
	public ResponseEntity<Void> deleteEmp(@PathVariable int empId) {
		empService.deleteEmp(empId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	

}
