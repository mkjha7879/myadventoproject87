package com.advt.secondms.exception;

public class ResourceNotFoundExcption extends RuntimeException {

	public ResourceNotFoundExcption(String message) {
		super(message);

	}
}
