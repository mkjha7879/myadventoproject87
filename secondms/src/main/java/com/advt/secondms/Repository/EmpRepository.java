package com.advt.secondms.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.advt.secondms.modal.Emp;

public interface EmpRepository extends JpaRepository<Emp, Integer> {

}
