package com.advt.secondms.global_excption_handler;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.advt.secondms.exception.ResourceNotFoundExcption;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(ResourceNotFoundExcption.class)
	public String handlerResourceNotFound(ResourceNotFoundExcption excption) {
		return  excption.getMessage();
	}

}
