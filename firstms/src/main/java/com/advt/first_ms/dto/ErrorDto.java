package com.advt.first_ms.dto;

public class ErrorDto {
	private String errCode;
	private String errMessage;

	public ErrorDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ErrorDto(String errCode, String errMessage) {
		super();
		this.errCode = errCode;
		this.errMessage = errMessage;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

}
