package com.advt.first_ms.exception;

public class UnExpectedCustomException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String errMessage;

	public UnExpectedCustomException(String errMessage) {
		super(errMessage);

		this.errMessage = errMessage;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
