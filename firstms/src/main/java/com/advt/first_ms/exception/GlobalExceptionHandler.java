package com.advt.first_ms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.advt.first_ms.dto.ErrorDto;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler({ SortNotSupportedException.class, InputNotSupportedException.class })
	public ResponseEntity<ErrorDto> handlerSortInvalidInputEx(RuntimeException exception) {
		String errCode = "";
		String errMessafe = "";
		if (exception instanceof SortNotSupportedException) {
			SortNotSupportedException exception2 = (SortNotSupportedException) exception;
			errCode = exception2.getErrCode();
			errMessafe = exception2.getErrMessage();
		} else if (exception instanceof InputNotSupportedException) {
			InputNotSupportedException exception1 = (InputNotSupportedException) exception;
			errCode = exception1.getErrCode();
			errMessafe = exception1.getErrMessage();
		}
		ErrorDto dto = new ErrorDto(errCode, errMessafe);
		return new ResponseEntity<ErrorDto>(dto, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(ResourceNotFound.class)
	public ResponseEntity<ErrorDto> handlerResourceNotFoundEx(ResourceNotFound found) {
		ErrorDto dto = new ErrorDto(found.getErrCode(), found.getErrMessage());
		return new ResponseEntity<ErrorDto>(dto, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UnExpectedCustomException.class)
	public ResponseEntity<ErrorDto> handleUnExpectedEx(UnExpectedCustomException customException) {
		String errCode = "";
		String errMess = customException.getErrMessage();
		ErrorDto dto = new ErrorDto(errCode, errMess);
		return new ResponseEntity<ErrorDto>(dto, HttpStatus.SERVICE_UNAVAILABLE);

	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDto> handlerException(Exception exception) {
		String errCode = "FSW1004";
		String errMessage = exception.getMessage();
		ErrorDto dto = new ErrorDto(errCode, errMessage);
		return new ResponseEntity<ErrorDto>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
