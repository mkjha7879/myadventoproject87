package com.advt.first_ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advt.first_ms.dto.Employee;
import com.advt.first_ms.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	@Qualifier(value = "employeeImpl")
	private EmployeeService employeeService;

	@GetMapping("/getAll")
	public ResponseEntity<List<Employee>> getAllEmployee(@RequestParam(value = "sortby") String sortBy) {
		List<Employee> allStudent = employeeService.getAllEmployees(sortBy);
		return new ResponseEntity<List<Employee>>(allStudent, HttpStatus.ACCEPTED);
	}

	@PostMapping("addEmployee")
	public ResponseEntity<Employee> create(@RequestBody Employee employee) {
		Employee addEmployee = employeeService.addEmployee(employee);
		return new ResponseEntity<Employee>(addEmployee, HttpStatus.CREATED);
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Employee> delete(@PathVariable int id) {
		Employee employee = employeeService.getEmployee(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@PutMapping("/updateEmployee/{empId}")
	public ResponseEntity<Void> updateEmployee(@PathVariable int empId, @RequestBody Employee employee) {
		employeeService.updateEmployee(empId, employee);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@DeleteMapping("/deleteEmployee/{id}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable int id) {
		employeeService.deleteEmployee(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
