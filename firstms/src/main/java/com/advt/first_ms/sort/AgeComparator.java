package com.advt.first_ms.sort;

import java.util.Comparator;

import com.advt.first_ms.dto.Employee;

public class AgeComparator implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		return o1.getAge()-o2.getAge();
	}

}
