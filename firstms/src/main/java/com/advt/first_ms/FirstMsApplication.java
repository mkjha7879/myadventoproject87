package com.advt.first_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstMsApplication.class, args);
	}

}
