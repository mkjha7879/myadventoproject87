package com.advt.first_ms.serviceimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.advt.first_ms.connection.DataBaseConnection;
import com.advt.first_ms.dto.Employee;
import com.advt.first_ms.exception.InputNotSupportedException;
import com.advt.first_ms.exception.ResourceNotFound;
import com.advt.first_ms.exception.SortNotSupportedException;
import com.advt.first_ms.exception.UnExpectedCustomException;
import com.advt.first_ms.service.EmployeeService;
import com.advt.first_ms.sort.AgeComparator;
import com.advt.first_ms.sort.NameComparator;

@Service(value = "employeeImpl")
public class EmployeeServiceImpl implements EmployeeService {
	@Override
	public List<Employee> getAllEmployees(String sortBy) {
		List<Employee> employees = new ArrayList<>();
		Connection con = null;
		try {
			con = DataBaseConnection.getConnection();
			String query = "SELECT * FROM employee ";
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int empId = rs.getInt("emp_id");
				String name = rs.getString("emp_name");
				int age = rs.getInt("age");
				float salary = rs.getFloat("salary");
				Employee employee = new Employee(empId, name, age, salary);
				employees.add(employee);
			}
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpected error");
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpected error");
				}
			}
		}
		sortBy(employees, sortBy);
		return employees;
	}

	public static void sortBy(List<Employee> employees, String key) {
		key = key.toLowerCase();
		switch (key) {
		case "name":
			Collections.sort(employees, new NameComparator());

			break;
		case "age":
			Collections.sort(employees, new AgeComparator());

			break;

		default:
			throw new SortNotSupportedException("FMS101", "Given sort is not supported in");
		}
	}

	@Override
	public Employee getEmployee(int id) {
		Employee employee = null;
		Connection con = null;
		try {
			con = DataBaseConnection.getConnection();
			String query = "SELECT * FROM employee  where emp_id =?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int empId = rs.getInt("emp_id");
				String name = rs.getString("emp_name");
				int age = rs.getInt("age");
				float salary = rs.getFloat("salary");
				employee = new Employee(empId, name, age, salary);
			}
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpected error");
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new UnExpectedCustomException("Unexpected error");
				}
			}

		}
		if (employee != null) {
			return employee;
		}

		throw new ResourceNotFound("FSW103", "the" + id + "  which you are looking is not found");
	}

	@Override
	public Employee addEmployee(Employee employee) {
		Employee employee2 = null;
		Connection con = null;
		try {
			con = DataBaseConnection.getConnection();
			String query = "INSERT INTO employee(emp_name,age,salary) values (?,?,?)";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, employee.getName());
			ps.setInt(2, employee.getAge());
			ps.setFloat(3, employee.getSalary());
			int executeUpdate = ps.executeUpdate();
			if (executeUpdate == 1) {
				// employee2.setId(employee.getId());
				employee2 = employee;
			} else {
				employee2 = null;
			}
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpected error");
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpected error");
				}
			}
		}
		return employee2;
	}

	@Override
	public void updateEmployee(int id, Employee employee) {
		Connection con = null;
		if (getEmployee(id) != null) {
			try {
				con = DataBaseConnection.getConnection();
				String query = "UPDATE employee set emp_Name=?,age=?,salary=? where emp_id=?";
				PreparedStatement ps = con.prepareStatement(query);
				ps.setString(1, employee.getName());
				ps.setInt(2, employee.getAge());
				ps.setFloat(3, employee.getSalary());
				ps.setInt(4, id);
				ps.executeUpdate();
			} catch (SQLException e) {
				throw new UnExpectedCustomException("Unexpected error");
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						throw new UnExpectedCustomException("Unexpected error");
					}
				}
			}
		}

	}

	@Override
	public void updateSelEmployee(int id, Employee employee) {
		Connection con = null;
		try {
			con = DataBaseConnection.getConnection();
			String query = "UPDATE employee set emp_Name=? where emp_id=?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, employee.getName());
			ps.setInt(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpected error");
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpected error");
				}
			}
		}

	}

	@Override
	public void deleteEmployee(int id) {
		if (getEmployee(id) != null) {
			Connection con = null;
			try {
				con = DataBaseConnection.getConnection();
				String query = "DELETE FROM employee  where emp_id =?";
				PreparedStatement ps = con.prepareStatement(query);
				ps.setInt(1, id);
				int executeUpdate = ps.executeUpdate();
			} catch (SQLException e) {
				throw new UnExpectedCustomException("Unexpected error");
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						throw new UnExpectedCustomException("Unexpected error");
					}
				}
			}
		}

	}

}
