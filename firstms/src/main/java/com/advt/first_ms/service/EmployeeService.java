package com.advt.first_ms.service;

import java.util.List;

import com.advt.first_ms.dto.Employee;


public interface EmployeeService {
	public List<Employee> getAllEmployees(String sortBy);

	public Employee getEmployee(int empId);

	public Employee addEmployee(Employee employee);

	public void updateEmployee(int id, Employee employee);

	public void updateSelEmployee(int id, Employee employee);

	public void deleteEmployee(int id);
}
